import datetime

from sqlalchemy import text
from sqlalchemy.dialects.postgresql import UUID, JSONB
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy import MetaData, Column, Date, DateTime, ForeignKey, Integer, String, UniqueConstraint, Text, Boolean

SCHEMA_NAME = 'public'

metadata = MetaData(schema=SCHEMA_NAME)
Base = declarative_base(metadata=metadata)


class TableBase(Base):
    __abstract__ = True

    id = Column(Integer, primary_key=True, nullable=False, unique=True, index=True)
    identifier = Column(UUID(as_uuid=True), unique=True, index=True, nullable=False, server_default=text('gen_random_uuid()'))
    date_created = Column(DateTime, server_default=text("timezone('utc', now())"), nullable=False, unique=False, index=True)
    date_modified = Column(DateTime, server_default=text("timezone('utc', now())"), nullable=False, unique=False, index=True)

    @declared_attr
    def __foreign_key__(self):
        return f'{self.__tablename__}.id'


class ResourceType(TableBase):
    __tablename__ = 'resource_type'

    name = Column(String(255), nullable=False, unique=True, index=True)


class Resource(TableBase):
    __tablename__ = 'resource'

    resource_type_id = Column(Integer, ForeignKey(ResourceType.__foreign_key__), nullable=False, index=True)
    object_id = Column(Integer, nullable=False, index=True)
    is_archived = Column(Boolean, server_default=text("FALSE"), index=True)
    date_archived = Column(DateTime, nullable=True, index=True, server_default=text("NULL"))


class User(TableBase):
    __tablename__ = 'user'

    @declared_attr
    def last_user_involved_id(self):
        return Column(Integer, ForeignKey(self.id))

    resource_id = Column(Integer, ForeignKey(Resource.__foreign_key__), nullable=False, index=True)
    email = Column(String(255), unique=True, index=True, nullable=False)
    password_hash = Column(String(255), index=False, unique=False, nullable=False)
    password_salt = Column(String(255), index=False, unique=True, nullable=False)
    password_algo = Column(String(255), index=True, unique=False, nullable=False)
    password_iter = Column(Integer, index=True, unique=False, nullable=False)
    password_set = Column(Boolean, index=True, unique=False, nullable=False, server_default=text("FALSE"))


class ResourceHistory(TableBase):
    __tablename__ = 'resource_history'

    resource_id = Column(Integer, ForeignKey(Resource.__foreign_key__), nullable=False, index=True)
    user_id = Column(Integer, ForeignKey(User.__foreign_key__), nullable=True, index=True)
    date_archived = Column(DateTime, default=None, nullable=True, index=True)
    resource_data = Column(JSONB, nullable=False)


class ModelBase(TableBase):
    __abstract__ = True

    @declared_attr
    def resource_id(self):
        return Column(Integer, ForeignKey(Resource.__foreign_key__), nullable=False, index=True)

    @declared_attr
    def last_user_involved_id(self):
        return Column(Integer, ForeignKey(User.__foreign_key__), nullable=False, index=True)
