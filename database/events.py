from sqlalchemy import DDL
from sqlalchemy import event

from . import schema
from .triggers import (
    create_resource_type_entry_function,
    set_resource_history_on_resource_archive,
    update_resource_modified_timestamp_before_UPDATE,
    update_resource_modified_timestamp_before_DELETE,
    create_resource_versions_trigger_function_on_DELETE,
    create_resource_versions_trigger_function_on_INSERT,
    create_resource_versions_trigger_function_on_UPDATE,
    create_resource_history_triggers,
    create_resource_trigger_function_before_INSERT,
    create_resource_archive_trigger_function,
)

def set_triggers_for_all_tables(connection):
    create_resource_type_entry_function(connection)
    create_resource_archive_trigger_function(connection)

    schema.metadata.reflect(connection)

    for Table in schema.metadata.tables.values():
        if Table.name not in [
            'alembic_version',
            schema.ResourceType.__tablename__,
            schema.ResourceHistory.__tablename__,
            schema.Resource.__tablename__
        ]:
            create_resource_trigger_function_before_INSERT(connection, Table)
            create_resource_versions_trigger_function_on_INSERT(connection, Table)
            update_resource_modified_timestamp_before_UPDATE(connection, Table)
            create_resource_versions_trigger_function_on_UPDATE(connection, Table)
            update_resource_modified_timestamp_before_DELETE(connection, Table)
            create_resource_versions_trigger_function_on_DELETE(connection, Table)
            create_resource_history_triggers(connection, Table)
