"""Setup database

Revision ID: 7c81e98b038d
Revises:
Create Date: 2019-10-07 16:17:13.461751

"""
from alembic import op
import sqlalchemy as sa

from database.schema import SCHEMA_NAME

# revision identifiers, used by Alembic.
revision = '7c81e98b038d'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.execute(f"""CREATE EXTENSION IF NOT EXISTS pgcrypto;""")

    # https://dba.stackexchange.com/a/165923/39030 Why we have this domain thing
    # https://stackoverflow.com/a/42521252/1931039 Why the regex has 2 curly braces
    op.execute(f"""
    CREATE EXTENSION IF NOT EXISTS citext;
    CREATE DOMAIN {SCHEMA_NAME}.email_address AS citext
    CHECK ( value ~ '^[a-zA-Z0-9.!#$%&''*+/=?^_`{{|}}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{{0,61}}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{{0,61}}[a-zA-Z0-9])?)*$' );
    """)

def downgrade():
    pass
