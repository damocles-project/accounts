from . import schema


def create_resource_type_entry_function(connection):
    connection.execute(f"""
    CREATE OR REPLACE FUNCTION create_resource_type_entry(resource_name text)
    RETURNS INTEGER as
    $BODY$
        DECLARE
            PK INTEGER;
        BEGIN
            IF (
                SELECT count(name)
                FROM "{schema.ResourceType.__tablename__}"
                WHERE name LIKE resource_name LIMIT 1
                ) < 1 THEN
                    INSERT INTO "{schema.ResourceType.__tablename__}" (name) VALUES (resource_name);
            END IF;

            SELECT id INTO PK
            FROM "{schema.ResourceType.__tablename__}"
            WHERE name LIKE resource_name LIMIT 1;

            RETURN PK;

        END;
    $BODY$
    LANGUAGE plpgsql VOLATILE COST 100;
    """)


def create_resource_trigger_function_before_INSERT(connection, Table):
    connection.execute(
        f"""
    CREATE OR REPLACE FUNCTION create_resource_before_insert_for_{Table.name}()
    RETURNS trigger AS
    $BODY$
    DECLARE
        RESOURCE_ID INTEGER;
        RESOURCE_TYPE_ID INTEGER;
    BEGIN
        SELECT create_resource_type_entry('{Table.name}') INTO RESOURCE_TYPE_ID;

        INSERT INTO "{schema.Resource.__tablename__}"(resource_type_id, object_id)
        VALUES (RESOURCE_TYPE_ID, NEW.id)
        RETURNING id INTO RESOURCE_ID;

        NEW.resource_id = RESOURCE_ID;

        RETURN NEW;
    END
    $BODY$
    LANGUAGE plpgsql VOLATILE COST 100;
    """
    )


def create_resource_versions_trigger_function_on_INSERT(connection, Table):
    connection.execute(
        f"""
    CREATE OR REPLACE FUNCTION create_resource_history_on_insert_for_{Table.name}()
    RETURNS trigger AS
    $BODY$
    BEGIN

        INSERT INTO "{schema.ResourceHistory.__tablename__}"(date_created, resource_id, user_id, resource_data)
        VALUES (
            NEW.date_created,
            NEW.resource_id,
            NEW.last_user_involved_id,
            row_to_json(NEW)
        );
        RETURN NEW;
    END
    $BODY$
    LANGUAGE plpgsql VOLATILE COST 100;
    """
    )


def update_resource_modified_timestamp_before_UPDATE(connection, Table):
    connection.execute(
        f"""
    CREATE OR REPLACE FUNCTION update_resource_timestamp_on_insert_for_{Table.name}()
    RETURNS TRIGGER AS $$
    BEGIN
        NEW.date_modified = now()::timestamp;
        RETURN NEW;
    END;
    $$ language 'plpgsql';
    """
    )


def create_resource_versions_trigger_function_on_UPDATE(connection, Table):
    connection.execute(
        f"""
    CREATE OR REPLACE FUNCTION create_resource_history_on_update_for_{Table.name}()
    RETURNS trigger AS
    $BODY$
    DECLARE
        RESOURCE_TYPE_ID  INTEGER;
    BEGIN
        SELECT create_resource_type_entry('{Table.name}') INTO RESOURCE_TYPE_ID;

        UPDATE "{schema.ResourceHistory.__tablename__}"
        SET date_archived = NEW.date_modified
        WHERE
            date_archived is null
        and resource_id = NEW.resource_id;

        INSERT INTO "{schema.ResourceHistory.__tablename__}" (resource_id, user_id, resource_data)
        VALUES (
            NEW.resource_id,
            NEW.last_user_involved_id,
            row_to_json(NEW)
        );

        RETURN NEW;
    END
    $BODY$
    LANGUAGE plpgsql VOLATILE COST 100;
    """
    )


def update_resource_modified_timestamp_before_DELETE(connection, Table):
    connection.execute(
        f"""
    CREATE OR REPLACE FUNCTION update_resource_timestamp_on_delete_for_{Table.name}()
    RETURNS TRIGGER AS $$
    BEGIN
        OLD.date_modified = now()::timestamp;
        RETURN OLD;
    END;
    $$ language 'plpgsql';
    """
    )


def create_resource_versions_trigger_function_on_DELETE(connection, Table):
    # All tables
    connection.execute(
        f"""
    CREATE OR REPLACE FUNCTION update_resource_history_on_delete_for_{Table.name}()
    RETURNS trigger AS
    $BODY$
    DECLARE
        RESOURCE_TYPE_ID  INTEGER;
    BEGIN
        SELECT create_resource_type_entry('{Table.name}') INTO RESOURCE_TYPE_ID;

        UPDATE "{schema.ResourceHistory.__tablename__}"
        SET date_archived = now()::timestamp
        WHERE
            date_archived is null
        and resource_id = OLD.resource_id;

        RETURN OLD;
    END
    $BODY$
    LANGUAGE plpgsql VOLATILE COST 100;
    """
    )


def create_resource_archive_trigger_function(connection):
    # All tables
    connection.execute(
        f"""
    CREATE OR REPLACE FUNCTION set_resource_history_on_resource_archive()
    RETURNS trigger AS
    $BODY$
    DECLARE
        MOMENT timestamp := now()::timestamp;
    BEGIN
        IF OLD.is_archived = FALSE and NEW.is_archived = TRUE THEN
            UPDATE  "{schema.ResourceHistory.__tablename__}"
            SET     date_archived = MOMENT
            WHERE   date_archived is null
            AND     resource_id = OLD.resource_id;

            NEW.date_archived = MOMENT;

        END IF;

        RETURN NEW;
    END
    $BODY$
    LANGUAGE plpgsql VOLATILE COST 100;
    """
    )


def set_resource_history_on_resource_archive(connection):
    connection.execute(
        f"""
        DROP TRIGGER IF EXISTS update_resource_history_on_archive ON {schema.SCHEMA_NAME}.{schema.Resource.__tablename__};
        CREATE TRIGGER update_resource_history_on_archive
        BEFORE UPDATE
        ON "{schema.Resource.__tablename__}"
        FOR EACH ROW
        EXECUTE PROCEDURE set_resource_history_on_resource_archive();
        """
    )



def create_resource_history_triggers(connection, Table):
    # https://stackoverflow.com/a/24089729/1931039
    connection.execute(
        f"""
        do $$
        BEGIN
            IF to_regclass('{schema.SCHEMA_NAME}.{Table.name}') IS NOT NULL THEN
                DROP TRIGGER IF EXISTS update_resource_modified_at_timestamp_on_UPDATE ON {schema.SCHEMA_NAME}.{Table.name};
                CREATE TRIGGER update_resource_modified_at_timestamp_on_UPDATE
                BEFORE UPDATE
                ON {schema.SCHEMA_NAME}.{Table.name}
                FOR EACH ROW
                EXECUTE PROCEDURE update_resource_timestamp_on_insert_for_{Table.name}();
            END IF;
        END;
        $$
        """
    )

    connection.execute(
        f"""
        do $$
        BEGIN
            IF to_regclass('{schema.SCHEMA_NAME}.{Table.name}') IS NOT NULL THEN
                DROP TRIGGER IF EXISTS update_resource_modified_at_timestamp_on_DELETE ON {schema.SCHEMA_NAME}.{Table.name};
                CREATE TRIGGER update_resource_modified_at_timestamp_on_DELETE
                BEFORE DELETE
                ON {schema.SCHEMA_NAME}.{Table.name}
                FOR EACH ROW
                EXECUTE PROCEDURE update_resource_timestamp_on_delete_for_{Table.name}();
            END IF;
        END;
        $$
        """
    )

    connection.execute(
        f"""
        do $$
        BEGIN
            IF to_regclass('{schema.SCHEMA_NAME}.{Table.name}') IS NOT NULL THEN
                DROP TRIGGER IF EXISTS update_resource_history_on_DELETE ON {schema.SCHEMA_NAME}.{Table.name};

                CREATE TRIGGER update_resource_history_on_DELETE
                BEFORE DELETE
                ON {schema.SCHEMA_NAME}.{Table.name}
                FOR EACH ROW
                EXECUTE PROCEDURE update_resource_history_on_delete_for_{Table.name}();
            END IF;
        END;
        $$
        """
    )

    connection.execute(
        f"""
        do $$
        BEGIN
            IF to_regclass('{schema.SCHEMA_NAME}.{Table.name}') IS NOT NULL THEN
                DROP TRIGGER IF EXISTS create_resource_before_INSERT ON {schema.SCHEMA_NAME}.{Table.name};

                CREATE TRIGGER create_resource_before_INSERT
                BEFORE INSERT
                ON {schema.SCHEMA_NAME}.{Table.name}
                FOR EACH ROW
                EXECUTE PROCEDURE create_resource_before_insert_for_{Table.name}();
            END IF;
        END;
        $$
        """
    )

    connection.execute(
        f"""
        do $$
        BEGIN
            IF to_regclass('{schema.SCHEMA_NAME}.{Table.name}') IS NOT NULL THEN
                DROP TRIGGER IF EXISTS create_resource_history_on_INSERT ON {schema.SCHEMA_NAME}.{Table.name};

                CREATE TRIGGER create_resource_history_on_INSERT
                AFTER INSERT
                ON {schema.SCHEMA_NAME}.{Table.name}
                FOR EACH ROW
                EXECUTE PROCEDURE create_resource_history_on_insert_for_{Table.name}();
            END IF;
        END;
        $$
        """
    )

    connection.execute(
        f"""
        do $$
        BEGIN
            IF to_regclass('{schema.SCHEMA_NAME}.{Table.name}') IS NOT NULL THEN
                DROP TRIGGER IF EXISTS create_resource_history_on_UPDATE ON {schema.SCHEMA_NAME}.{Table.name};

                CREATE TRIGGER create_resource_history_on_UPDATE
                AFTER UPDATE
                ON {schema.SCHEMA_NAME}.{Table.name}
                FOR EACH ROW
                EXECUTE PROCEDURE create_resource_history_on_update_for_{Table.name}();
            END IF;
        END;
        $$
        """
    )
