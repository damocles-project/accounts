module Accounts.Users.Repository

open System
open System.Data.Common
open Accounts.Database
open Accounts.Users.Password


type DbUser = {
    id: int
    identifier: Guid
    date_created: DateTime
    date_modified: DateTime
    resource_id: int
    username: string
    password_hash: string
    password_salt: string
    password_algo: string
    password_iter: int
    last_user_involved_id: int
    } with

    static member sqlSelectAll =
        Utilities.extractParams<DbUser>()

    member this.toMap() =
        Utilities.toMap(this)


let getUserById (connection: DbConnection) (id: int) =
    let data = dict [
        "id" => id
    ]
    let sql =
        sprintf "
        SELECT   %s
        FROM     public.user
        WHERE    id = @id
        LIMIT 1;
        " DbUser.sqlSelectAll

    let result = Methods.QuerySingle<DbUser> connection sql (data |> Some)
    result


let getUserByUsername (connection: DbConnection) (username: string) =
    let data = dict [
        "username" => username
    ]
    let sql =
        sprintf "
        SELECT   %s
        FROM     public.user
        WHERE    username = @username
        LIMIT 1;
        " DbUser.sqlSelectAll

    let result = Methods.QuerySingle<DbUser> connection sql (data |> Some)
    result

let setUserPassword (connection: DbConnection) (user: DbUser) (password: string) =
    let credentials = hashPassword password
    let data = dict [
        "id" => user.id;
        "password_hash" => credentials.Pass
        "password_salt" => credentials.Salt
        "password_algo" => credentials.Algo
        "password_iter" => credentials.Iter
        "last_user_involved_id" => user.id
        ]

    let sql =
        @"
            UPDATE
                public.user
            SET
                password_hash         = @password_hash,
                password_salt         = @password_salt,
                password_algo         = @password_algo,
                password_iter         = @password_iter,
                last_user_involved_id = @last_user_involved_id
            WHERE
                id = @id
        "

    let result = Methods.QuerySingle<DbUser> connection sql (data |> Some)
    result


let updateUserProfile (connection: DbConnection) (username: string) =
    let data = dict [
        "username" => username
    ]
    let sql =
        @"
            UPDATE
                public.user
            SET
                username = @username
            WHERE
                id = @id
        "

    Methods.QuerySingle<DbUser> connection sql (data |> Some)
