module Accounts.Users.Password

open System
open Microsoft.AspNetCore.Cryptography.KeyDerivation
open System.Security.Cryptography


type PasswordComponents = {
    Salt: string;
    Algo: string;
    Pass: string;
    Iter: int;
}

let algo = "pbkdf2"
let iter = 100000

// Based on https://docs.microsoft.com/en-us/aspnet/core/security/data-protection/consumer-apis/password-hashing?view=aspnetcore-3.0
let createSalt() =
    let rng = RandomNumberGenerator.Create()
    let salt = Array.zeroCreate<byte> (128 / 8)
    rng.GetBytes salt

    salt

let hashPassword (password : string) : PasswordComponents =
    let salt = createSalt()
    let pass =
        KeyDerivation.Pbkdf2(
            password,
            salt,
            KeyDerivationPrf.HMACSHA512,
            iter,
            ( 256 / 8 ))
        |> Convert.ToBase64String

    {
        PasswordComponents.Salt=salt |> Convert.ToBase64String;
        PasswordComponents.Algo=algo;
        PasswordComponents.Iter=iter;
        PasswordComponents.Pass=pass;
    }

let comparePassword password (components: PasswordComponents) : bool =
    let testValue =
        KeyDerivation.Pbkdf2(
            password,
            components.Salt |> Convert.FromBase64String,
            KeyDerivationPrf.HMACSHA512,
            components.Iter,
            ( 256 / 8 ))
        |> Convert.ToBase64String

    testValue = components.Pass
