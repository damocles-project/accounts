module Accounts.Database

open Microsoft.FSharp.Reflection
open System.Collections.Generic
open System.Data.Common
open Dapper
open Npgsql


[<Literal>]
let ConnectionString =
    "Server=localhost;" +
    "Database=jabronis;" +
    "User Id=localdev;" +
    "Password=password;"


let databaseConnection () =
    let c = new NpgsqlConnection(ConnectionString)
    c.Open()
    c



module Utilities =
    let extractParams<'T> () =
        [
        for p in FSharpType.GetRecordFields(typeof<'T>) -> p.Name
        ] |> String.concat ", "


    let toMap (recd:'T) =
      [ for p in FSharpType.GetRecordFields(typeof<'T>) -> p.Name, p.GetValue(recd) ]
      |> Map.ofSeq


[<AutoOpen>]
module Operators =
    let inline (=>) a b = a, box b


[<AutoOpen>]
module Methods =

    let Execute (connection: DbConnection) (sql:string) (parameters:_) =
        try
            let result = connection.Execute(sql, parameters)
            Ok result
        with
        | ex -> Error ex


    let Query<'T> (connection:DbConnection) (sql:string) (parameters: IDictionary<string, obj> option) : Result<seq<'T>,exn> =
        try
            let result =
                match parameters with
                | Some p -> connection.Query<'T>(sql, p)
                | None -> connection.Query<'T>(sql)
            Ok result
        with
        | ex -> Error ex


    let QuerySingle<'T> (connection:DbConnection) (sql:string) (parameters:IDictionary<string, obj> option) =
        try
            let result =
                match parameters with
                | Some p -> connection.QuerySingleOrDefault<'T>(sql, p)
                | None -> connection.QuerySingleOrDefault<'T>(sql)

            if isNull (box result) then Ok None
            else Ok (Some result)

        with
        | ex -> Error ex


    let QueryAsync<'T> (connection:DbConnection) (sql:string) (parameters: IDictionary<string, obj> option) =
        async {
            try
                let! result =
                    match parameters with
                    | Some p -> connection.QueryAsync<'T>(sql, p)
                    | None -> connection.QueryAsync<'T>(sql)
                return Ok result
            with
            | ex -> return Error ex
        }


    let QuerySingleAsync<'T> (connection:DbConnection) (sql:string) (parameters:IDictionary<string, obj> option) =
        async {
            try
                let! result =
                    match parameters with
                    | Some p -> connection.QuerySingleOrDefaultAsync<'T>(sql, p)
                    | None -> connection.QuerySingleOrDefaultAsync<'T>(sql)

                if isNull (box result) then
                    return Ok None
                else
                    return Ok (Some result)

            with
            | ex -> return Error ex
        }
