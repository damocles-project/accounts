﻿namespace Accounts

open Suave

open Accounts.Database
open Accounts.Users


module Main =

    [<EntryPoint>]
    let main argv =
        let connection = databaseConnection()
        let optUser =
            match Repository.getUserById connection 1 with
                | Error _ -> failwith "error"
                | Ok data -> data

        match optUser with
        | None -> printfn "No user"
        | Some user -> user.toMap() |> printfn "%A"

        startWebServer defaultConfig (Successful.OK "Accounts Started...")

        0 // return an integer exit code
